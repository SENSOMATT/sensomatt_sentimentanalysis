# Sensomatt_SentimentAnalysis

Sentiment Analysis is a use case of Natural Language Processing (NLP) and comes under the category of text classification. To put it simply, Sentiment Analysis involves classifying a text into various sentiments, such as positive or negative, Happy, Sad or Neutral, etc.

## Installing
```
pip install -r requirements.txt
```

```
python -m textblob.download_corpora
```

## Running

### CLI

run the following command in the terminal to get the sentiment of the text
```
python main.py --text "I love sensomatt!"
```

### API

run the following command in the terminal to start the API
```
python api.py
```

## Built with

* [TextBlob](https://textblob.readthedocs.io/en/dev/) - Sentiment analysis library
* [Matplotlib](https://matplotlib.org/) - Plotting library
* [Pandas](https://pandas.pydata.org/) - Data analysis library
* [NumPy](https://numpy.org/) - Scientific computing library
