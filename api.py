from textblob import TextBlob
from textblob.classifiers import NaiveBayesClassifier
from fastapi import FastAPI, Form, HTTPException, Request
import uvicorn
import dataset


app = FastAPI()


@app.get("/")
def root():
    return {
        "Title": "Sensomatt sentiment analysis API",
        "Description": "This API is used to classifying a text into various sentiments, such as positive or negative, Happy, Sad or Neutral, etc."
    }


@app.post("/api/sentiment-analysis")
async def sentiment_analysis(text: str = Form(...)):
    if not text:
        raise HTTPException(
            status_code=400, detail="Invalid text. Please enter a valid text.")

    try:
        if cl is None:
            blob = TextBlob(text)
            sentiment = blob.sentiment
            polarity = round(sentiment.polarity, 3)
            subjectivity = round(sentiment.subjectivity, 3)
            return {"polarity": polarity, "subjectivity": subjectivity}
        else:
            sentiment = cl.classify(text)
            return {"sentiment": sentiment}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@app.post("/api/get-words")
async def get_words(text: str = Form(...)):
    if not text:
        raise HTTPException(
            status_code=400, detail="Invalid text. Please enter a valid text.")

    try:
        blob = TextBlob(text)
        words = blob.words
        return {"words": words}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@app.post("/api/get-sentences")
async def get_sentences(text: str = Form(...)):
    if not text:
        raise HTTPException(
            status_code=400, detail="Invalid text. Please enter a valid text.")

    try:
        blob = TextBlob(text)
        sentences = blob.sentences
        return {"sentences": sentences}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@app.post("/api/get-tags")
async def get_tags(text: str = Form(...)):
    if not text:
        raise HTTPException(
            status_code=400, detail="Invalid text. Please enter a valid text.")

    try:
        blob = TextBlob(text)
        tags = blob.tags
        return {"tags": tags}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@app.post("/api/get-noun-phrases")
async def get_noun_phrases(text: str = Form(...)):
    if not text:
        raise HTTPException(
            status_code=400, detail="Invalid text. Please enter a valid text.")

    try:
        blob = TextBlob(text)
        noun_phrases = blob.noun_phrases
        return {"noun_phrases": noun_phrases}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@app.get("/api/train")
async def train():
    global cl

    try:
        cl = NaiveBayesClassifier(dataset.train)
        test_accuracy = cl.accuracy(dataset.test)
        return {"test_accuracy": test_accuracy}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@app.post("/api/update")
async def update(request: Request):
    global cl
    if not request:
        raise HTTPException(
            status_code=400, detail="Invalid data. Please enter a valid data.")
    if cl is None:
        raise HTTPException(
            status_code=400, detail="Please train the model first.")

    try:
        request_json = await request.json()
        new_data = request_json["new_data"]
        cl.update(new_data)
        test_accuracy = cl.accuracy(dataset.test)
        return {"test_accuracy": test_accuracy}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


if __name__ == "__main__":
    cl = None
    uvicorn.run(app, host="0.0.0.0", port=8000)
