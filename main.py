import argparse
from textblob import TextBlob
from textblob.classifiers import NaiveBayesClassifier
import dataset


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--text", type=str, default="Today is a beautiful day. Tomorrow looks like bad weather.")
    args = parser.parse_args()

    blob = TextBlob(args.text)
    print(blob.sentences)
    print(blob.words)
    print(blob.tags)
    print(blob.noun_phrases)
    print(blob.sentiment)
    print(round(blob.sentiment.polarity, 3))
    print(round(blob.sentiment.subjectivity, 3))
    print(blob.correct())
    print(blob.words[1].singularize())
    print(blob.words[1].pluralize())

    # Train
    cl = NaiveBayesClassifier(dataset.train)
    print(cl.classify("This is an amazing library!"))


if __name__ == "__main__":
    main()
